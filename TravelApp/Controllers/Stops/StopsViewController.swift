//
//  StopsViewController.swift
//  TravelApp
//
//  Created by Nikita on 25.06.2021.
//

import UIKit
import RealmSwift

class StopsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var stopsTableView: UITableView!
    
    var travel: RealmTravel!
    var stops: Results<RealmStop>!
    
    var stopsNotitficationToken: NotificationToken?  // позволит избавиться от замыканий делегатов - всего, что передает данные нахад.
    
    let cellSpacing: CGFloat = 5
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        stops = realm.objects(RealmStop.self).filter("travel == %@", travel.id) // %@ - означает, что в это место будет вставлено travel.id
        
        stopsNotitficationToken = stops.observe{ changes in // в этом месте мы подписались на изменения
            switch changes{
            
            case .initial(_):
                break
            case .update(_, deletions: let deletions, insertions: let insertions, modifications: let modifications):
                self.stopsTableView.reloadData()
            case .error(_):
                break
            }
        }
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addClicked(_sender:)))
        navigationItem.rightBarButtonItem = addButton
        
        let stopCellXib = UINib(nibName:"StopCell", bundle: nil)
        stopsTableView.register(stopCellXib, forCellReuseIdentifier: "StopCell")
        
        stopsTableView.delegate = self
        stopsTableView.dataSource = self
        stopsTableView.tableFooterView = UIView()
        
    }
    
    
    @objc func addClicked(_sender: Any) {
        
        let createStopVC = CreateStopViewController()
        createStopVC.travelId = travel.id
       
        self.navigationController?.pushViewController(createStopVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // высота ячейки
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacing
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell") as! StopCell
        let stop = stops[indexPath.row]
        cell.stopNameLabel.text = stop.name
        cell.stopDescriptionLabel.text = stop.desc
        cell.currencyLabel.text = stop.currency
        cell.spendingsStopLabel.text = "\(stop.spentMoney)"
        // закраска звездочек
        for i in 0..<cell.stars.count{
            if i < stop.rating{
                cell.stars[i].isHighlighted = true
            } else {
                cell.stars[i].isHighlighted = false
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                let createStopVC = CreateStopViewController()
                createStopVC.stop = stops[indexPath.row]
                createStopVC.travelId = travel.id
               
                self.navigationController?.pushViewController(createStopVC, animated: true)
    }
}
