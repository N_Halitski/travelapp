//
//  ServerManager.swift
//  TravelApp
//
//  Created by Nikita on 28.07.2021.
//

import Foundation
import FirebaseDatabase
// @escaping - перед замыканием - не надо его делать опциональным.
// будет два замыкания , второе on error, если ошибка будет
// for userId для конкретного юзера
class ServerManager{
    func dowloadTravels(for userId: String, onComplete: @escaping ([RealmTravel])->Void, onError: @escaping (String)-> Void){
        
        let database = Database.database().reference()
        // забиарем все путешесвтия по ключу юзер айдт лежит конкретная строка
        // в папке трэвел в выберет те путешесатия которые относится к этому юхер айди которым мы залогинились
        let child = database.child("travels").queryOrdered(byChild: "userId").queryEqual(toValue: userId)
        child.observeSingleEvent(of: .value) { response in
            if let value = response.value as? [String: Any]{
                var result: [RealmTravel] = []
                for item in value.values {
                    if let travelJson = item as? [String: Any]{
                        if let id = travelJson["id"] as? String,
                           let userId = travelJson["userId"] as? String,
                           let name = travelJson["name"] as? String,
                           let description = travelJson["description"] as? String{
                            
//                            let travel = Travel(id: id, userId: userId, name: name, description: description)
                            let travel = RealmTravel()
                            travel.id = id
                            travel.userId = userId
                            travel.name = name
                            travel.desc = description
                            result.append(travel)
                        }
                    }
                }
                // после того как цикл закончился - вызываю onComplete
                onComplete(result)
            } else {
                onError("Произошла ошибка при загрузке путешествий")
            }
        }
    }
    
    
    func sandTravelToServer(_ travel: RealmTravel) {
        // формируем словарик. будем там хранить все что нужно для травела с его данными для  базы данных
        // называем поля так, как мы будем их забирать и парсить
        let json = ["id": travel.id,
                    "userId": travel.userId,
                    "name": travel.name,
                    "description":travel.desc]
        let database = Database.database().reference()// ссылка на базу данных в firebase. он через googleinfoplist настраивает файлик
        let child = database.child("travels").child("\(travel.id)")  // в папку с travel по travel.id положи словарь json
        child.setValue(json) { error, ref in
        
        }
    }
    
    func sandStopToServer(_ stop: RealmStop) {
        // формируем словарик. будем там хранить все что нужно для остановки с его данными для  базы данных
        // называем поля так, как мы будем их забирать и парсить
        let json: [String: Any] = ["id": stop.id,
                    "travelId": stop.travelId,
                    "name": stop.name,
                    "description":stop.desc,
                    "longitude": stop.longitude,
                    "latitude": stop.latitude,
                    "currency": stop.currency,
                    "spentMoney": stop.spentMoney]
        let database = Database.database().reference()// ссылка на базу данных в firebase. он через googleinfoplist настраивает файлик
        
        let child = database.child("stops").child("\(stop.id)")  // в папку с stop по stop.id положи словарь json
        child.setValue(json) { error, ref in
        
        }
    }
    
    
    
    func downloadStops(forTravelId travelId: String, userId: String, onComplete: @escaping ([RealmStop])->Void) {
    
        let database = Database.database().reference()
        let child = database.child("stops").queryOrdered(byChild: "travelId").queryEqual(toValue: travelId)
        child.observeSingleEvent(of: .value) { responce in
            if let value = responce.value as? [String:Any]{
                var result: [RealmStop] = []
                for item in value.values {
                    if let stopJson = item as? [String:Any]{
                        if let id = stopJson["id"] as? String,
                           let travelId = stopJson["travelId"] as? String,
                           let name = stopJson["name"] as? String,
                           let description = stopJson["description"] as? String,
                           let longitude = stopJson["longitude"] as? Double,
                           let latitude = stopJson["latitude"] as? Double,
                           let currency = stopJson["currency"] as? String,
                           let spentMoney = stopJson["spentMoney"] as? String{
                            
                            let stop = RealmStop()
                            stop.id = id
                            stop.travelId = travelId
                            stop.name = name
                            stop.desc = description
                            stop.spentMoney = spentMoney
                            stop.currency = currency
                            stop.longitude = longitude
                            stop.latitude = latitude
                            result.append(stop)
                        }
                    }
                }
               onComplete(result)
            }
        }
    }
}
