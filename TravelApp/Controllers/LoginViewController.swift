//
//  LoginViewController.swift
//  TravelApp
//
//  Created by Nikita on 22.05.2021.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var topTitleLabelConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil) // нотификация, чтоо клава будет показана

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil) // нотификация, чтоо клава будет спрятана
    }
    
    
    @objc func keyboardDidShow (_ notification: Notification) {
        print("keyboardDidShow")
        topTitleLabelConstraint.constant = 24
    }
    
    @objc func keyboardDidHide (_ notification: Notification) {
        print("keyboardDidHide")
        topTitleLabelConstraint.constant = 232
    }
    
    @IBAction func eyeButtonClicked(_ sender: Any) {
        // MARK: Варианты проявления перечеркнутого глазика. ксли кнопка выбрана, то переключает на глаз без черты иначе оставляет как есть
        
        if eyeButton.isSelected {
            eyeButton.isSelected = false
            passwordTextField.isSecureTextEntry = false
        } else {
            eyeButton.isSelected = true
            passwordTextField.isSecureTextEntry = true
        }
        
        // MARK: Второй варинат проявления перечеркнутого глазика.
//        eyeButton.isSelected = !eyeButton.isSelected
        
        
        // MARK: Третий проявления перечеркнутого глазика.
      //  eyeButton.isSelected.toggle()
        
        
//        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            //  сервис firebase
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error" // если user nil то зайзет сюда
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert) // создали алерт
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil) // создали кнопку ОК
                    
                    alertVC.addAction(action) // добавили кнопку к алерту
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
           
        }
    }
}
