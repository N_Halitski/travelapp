//
//  RegistrationViewController.swift
//  TravelApp
//
//  Created by Nikita on 19.05.2021.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    @IBAction func registrationClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            //  сервис firebase
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error" // если user nil то зайзет сюда
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert) // создали алерт
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil) // создали кнопку ОК
                    
                    alertVC.addAction(action) // добавили кнопку к алерту
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
           
        }
    }
    
}
