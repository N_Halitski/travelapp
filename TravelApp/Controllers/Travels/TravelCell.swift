//
//  TravelCell.swift
//  TravelApp
//
//  Created by Nikita on 25.06.2021.
//

import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet var stars: [UIImageView]!
    
    @IBOutlet weak var travelNameLabel: UILabel!
    
    @IBOutlet weak var descriptionTravelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
    
}
