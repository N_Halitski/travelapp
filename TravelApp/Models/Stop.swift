//
//  Stops.swift
//  TravelApp
//
//  Created by Nikita on 25.06.2021.
//

import Foundation

class Stop {
    var id: String
    var travelId: String
    var name: String
    var description: String
    var cost: String
    var currency: String
    var longitude: Double
    var latitude: Double
    
    
    init(id: String, travelId: String, name: String, description: String, cost: String, currency: String, longitude: Double, latitude: Double ) {
        self.id = id
        self.travelId = travelId
        self.name = name
        self.description = description
        self.cost = cost
        self.currency = currency
        self.longitude = longitude
        self.latitude = latitude
    }
}
