//
//  ConfigureStopViewController.swift
//  TravelApp
//
//  Created by Nikita on 15.07.2021.
//

import UIKit
import RealmSwift



class CreateStopViewController: UIViewController, MapViewControllerDelegate {
    
    @IBOutlet weak var stopNameLabel: UITextField!
    @IBOutlet weak var stopDescription: UITextView!
    @IBOutlet weak var ratingOutlet: UILabel!
    @IBOutlet weak var currencySignLabel: UILabel!
    @IBOutlet weak var sumOfMoneySpentLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    
    var rating = 4
    var travelId: String = ""
    var stop: RealmStop?
    var serverManager = ServerManager()
    var spentMoney = ""
    var currency = ""
    var latitude: Double = 0
    var longitude: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let stop = stop{
            stopNameLabel.text = stop.name
            stopDescription.text = stop.desc
            if stop.longitude != 0, stop.latitude != 0 {
                longitudeLabel.text = "\(stop.longitude)"
                latitudeLabel.text = "\(stop.latitude)"
                longitude = stop.longitude
                latitude = stop.latitude
            }
            sumOfMoneySpentLabel.text = stop.spentMoney
            spentMoney = stop.spentMoney
            
            currencySignLabel.text = stop.currency
            currency = stop.currency
            ratingOutlet.text = "\(stop.rating)"
            rating = stop.rating
        
            
            
           
        }
        
        if stopNameLabel.text?.isEmpty != nil{
            if let name = stop?.name{
                stopNameLabel.text = name
            } else {
                stopNameLabel.text = ""
            }
        }
        
        if stopDescription.text.isEmpty != nil{
            if let description = stop?.description{
                stopDescription.text = description
            }
        } else {
            stopDescription.text = ""
        }
        
        if longitudeLabel.text?.isEmpty != nil{
            if let longitude = stop?.longitude,
               let latitude = stop?.latitude {
                longitudeLabel.text = "\(longitude)"
                latitudeLabel.text = "\(latitude)"
            }
        } else {
            longitudeLabel.text = ""
            latitudeLabel.text = ""
        }
        
        
        if sumOfMoneySpentLabel.text?.isEmpty != nil{
            
            sumOfMoneySpentLabel.text = ""
        }
        
        if currencySignLabel.text?.isEmpty != nil{
           
            currencySignLabel.text = ""
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector (addClicked))
    }
    
    @objc func addClicked(_sender: Any) {
        
        
        if let name = stopNameLabel.text,
           let description = stopDescription.text,
           let cost = sumOfMoneySpentLabel.text,
           let currency = currencySignLabel.text,
           let longitude = longitudeLabel.text,
           let latitude = latitudeLabel.text{
            let id = stop?.id ?? UUID().uuidString // обнови либо создай новый айди с новой остановкой
            
    
            let stop = RealmStop()
            stop.id = id
            stop.travelId = travelId
            stop.currency = currency
            stop.desc = description
            stop.name = name
            stop.spentMoney = cost
            stop.latitude = Double(latitude)!
            stop.longitude = Double(longitude)!
            stop.rating = rating
            // теперь после создания остановки нам нужно добавить её в Realm
            let realm = try! Realm()
            try! realm.write{
                
                realm.add(stop, update: .all)
            }
            serverManager.sandStopToServer(stop)
        }
  
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func chooseCurrencyButtonClicked(_ sender: Any) {
        let spentMoneyVC = SpentMoneyViewController()
        spentMoneyVC.spentMoney = self.spentMoney
        spentMoneyVC.saveClosure = { [weak self] (spentMoney, currency) in
            self?.spentMoney = spentMoney
            self?.currency = currency
            self?.sumOfMoneySpentLabel.text = spentMoney
            self?.currencySignLabel.text = currency
            
        }

        navigationController?.pushViewController(spentMoneyVC, animated: true)
        
    }
    
    @IBAction func mapClicked(_ sender: Any) {
        let mapController = MapViewController()
        mapController.delegate = self
        mapController.longitude = self.longitude
        mapController.latitude = self.latitude
        navigationController?.pushViewController(mapController, animated: true)
    }
    
    func mapControllerDidSelect(latitude: Double, longitude: Double) {
        longitudeLabel.text = String("\(longitude)")
        latitudeLabel.text = String("\(latitude)")
        self.longitude = longitude
        self.latitude = latitude
    }
    
    @IBAction func plusRatingClicked(_ sender: Any) {
        if rating < 5 {
            rating += 1
        }
        
        
        ratingOutlet.text = String(rating)
    }
    @IBAction func minusRatingClicked(_ sender: Any) {
        if rating > 1 {
            rating -= 1
        }
        ratingOutlet.text = String(rating)
    }
    
    
}
