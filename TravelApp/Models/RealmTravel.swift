//
//  RealmTravel.swift
//  TravelApp
//
//  Created by Nikita on 25.08.2021.
//

import UIKit
import RealmSwift

class RealmTravel: Object {
    @Persisted (primaryKey: true) var id: String = ""
    @Persisted var userId: String = ""
    @Persisted var name: String = ""
    @Persisted var desc: String = ""
}
