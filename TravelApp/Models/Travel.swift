//
//  Travel.swift
//  TravelApp
//
//  Created by Nikita on 23.06.2021.
//

import Foundation

class Travel {
    var userId: String
    var id: String
    var name: String
    var description: String
    var stops: [Stop]=[]
    
    init(id: String, userId:String, name: String, description:String) {
        self.userId = userId
        self.id = id
        self.name = name
        self.description = description
    }
    
}
