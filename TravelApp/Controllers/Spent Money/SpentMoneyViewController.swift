//
//  MoneySpentViewController.swift
//  TravelApp
//
//  Created by Nikita on 16.07.2021.
//

import UIKit

class SpentMoneyViewController: UIViewController {
    @IBOutlet weak var dollarOutlet: UIButton!
    @IBOutlet weak var euroOutlet: UIButton!
    @IBOutlet weak var rubleOutlet: UIButton!
    @IBOutlet weak var sumOfMoneyTextField: UITextField!
    
    var saveClosure: ((_ value: String,_ currency: String) -> Void)?
  
    var spentMoney = ""
    var currency = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
     
            
            sumOfMoneyTextField.text = spentMoney
        
        
        if currency == "$"{
            dollarOutlet.backgroundColor = UIColor(hexString: "#8389E8")
            dollarOutlet.tintColor = .white
            
            euroOutlet.backgroundColor = UIColor.white
            euroOutlet.tintColor = UIColor(hexString: "#8389E8")
            
            rubleOutlet.backgroundColor = UIColor.white
            rubleOutlet.tintColor = UIColor(hexString: "#8389E8")
        } else if currency == "€" {
            euroOutlet.backgroundColor = UIColor(hexString: "#8389E8")
            euroOutlet.tintColor = .white
            
            dollarOutlet.backgroundColor = UIColor.white
            dollarOutlet.tintColor = UIColor(hexString: "#8389E8")
            
            rubleOutlet.backgroundColor = UIColor.white
            rubleOutlet.tintColor = UIColor(hexString: "#8389E8")
        } else if currency == "₽"{
            rubleOutlet.backgroundColor = UIColor(hexString: "#8389E8")
            rubleOutlet.tintColor = .white
            
            dollarOutlet.backgroundColor = UIColor.white
            dollarOutlet.tintColor = UIColor(hexString: "#8389E8")
            
            euroOutlet.backgroundColor = UIColor.white
            euroOutlet.tintColor = UIColor(hexString: "#8389E8")
        }
        
    }
    
    
//    @IBAction func sumOfMoneyClicked(_ sender: Any) {
//        
//        if let sumOfMoney = sumOfMoneyTextField.text {
//            sumClos
//            print("Закомитил")
//        }
//    }
    
    @IBAction func dollarButtonClicked(_ sender: Any) {
       currency = "$"
        dollarOutlet.backgroundColor = UIColor(hexString: "#8389E8")
        dollarOutlet.tintColor = .white
        
        euroOutlet.backgroundColor = UIColor.white
        euroOutlet.tintColor = UIColor(hexString: "#8389E8")
        
        rubleOutlet.backgroundColor = UIColor.white
        rubleOutlet.tintColor = UIColor(hexString: "#8389E8")
        
    }
    @IBAction func euroButtonClicked(_ sender: Any) {
       currency = "€"
        euroOutlet.backgroundColor = UIColor(hexString: "#8389E8")
        euroOutlet.tintColor = .white
        
        dollarOutlet.backgroundColor = UIColor.white
        dollarOutlet.tintColor = UIColor(hexString: "#8389E8")
        
        rubleOutlet.backgroundColor = UIColor.white
        rubleOutlet.tintColor = UIColor(hexString: "#8389E8")

        
    }
    @IBAction func rubbleButtonClicked(_ sender: Any) {
        currency = "₽"
        rubleOutlet.backgroundColor = UIColor(hexString: "#8389E8")
        rubleOutlet.tintColor = .white
        
        dollarOutlet.backgroundColor = UIColor.white
        dollarOutlet.tintColor = UIColor(hexString: "#8389E8")
        
        euroOutlet.backgroundColor = UIColor.white
        euroOutlet.tintColor = UIColor(hexString: "#8389E8")
        
    }
    
    @IBAction func doneButtonClecked(_ sender: Any) {
    
        saveClosure?(sumOfMoneyTextField.text ?? "0", currency)
        navigationController?.popViewController(animated: true)
    }
    
}
