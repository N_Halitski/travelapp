//
//  MapViewController.swift
//  TravelApp
//
//  Created by Nikita on 17.07.2021.
//

import UIKit
import MapKit


protocol MapViewControllerDelegate {
    func mapControllerDidSelect(latitude: Double, longitude: Double) // 1
}

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    var delegate: MapViewControllerDelegate? //2
    var latitude: Double = 0
    var longitude: Double = 0
    var stop: Stop?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if latitude != nil, longitude != nil{
            let annotation = MKPointAnnotation()
                annotation.coordinate = .init(latitude: latitude, longitude: longitude)
                mapView.addAnnotation(annotation)
        
        } else {
           return
        }
        
    }
    
    @IBAction func longTap(_ sender: UIGestureRecognizer) {
        
        switch sender.state {
        
        case .possible:
            break
        case .began:
            mapView.removeAnnotations(mapView.annotations) // чтобы он удалял предыдущуу отметку
            // добавляем крансый маркер на карту
            let locationInView = sender.location(in: mapView)
            // приобразовываем координату в реальную координату
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            //создаем красную штукень
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationOnMap // добавили координаты локации
            mapView.addAnnotation(annotation)//  добавили красную отметку
            
              latitude = locationOnMap.latitude
             longitude = locationOnMap.longitude
            
            
        
                // MARK: сообщить делегату
            delegate?.mapControllerDidSelect(latitude: locationOnMap.latitude, longitude: locationOnMap.longitude)
            
        case .changed:
            break
        case .ended:
            break
        case .cancelled:
            break
        case .failed:
            break
        @unknown default:
            break
        }
    }
}
