//
//  SplashViewController.swift
//  TravelApp
//
//  Created by Nikita on 01.09.2021.
//

import UIKit
import FirebaseAuth

class SplashViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){ [weak self] in // используем dispatchqueue так как нам нужно подождать 2 секнуды 
            guard let self = self else {return}
            if Auth.auth().currentUser?.uid != nil{ // значит я залогинин
                let travelVC = TravelListViewController()
                self.navigationController?.pushViewController(travelVC, animated: true)
            } else { // иначе на экран welcome где переходим на жкрна регистрации
                let welcomeVC = self.storyboard?.instantiateViewController(identifier: "WelcomeViewController") as! WelcomeViewController
                self.navigationController?.pushViewController(welcomeVC, animated: true)
            }
            
        }
        
    }
    


}
