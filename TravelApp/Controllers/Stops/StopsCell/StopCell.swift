//
//  StopCell.swift
//  TravelApp
//
//  Created by Nikita on 25.06.2021.
//

import UIKit

class StopCell: UITableViewCell {
    
    @IBOutlet weak var stopNameLabel: UILabel!
    @IBOutlet weak var stopDescriptionLabel: UILabel!
    @IBOutlet weak var spendingsStopLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet var stars: [UIImageView]!
    
    var travel :Travel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    
}
