//
//  RealmStop.swift
//  TravelApp
//
//  Created by Nikita on 28.08.2021.
//

import Foundation
import RealmSwift

class RealmStop: Object {
    @Persisted (primaryKey: true) var id: String  = ""
    @Persisted var travelId: String  = ""
    @Persisted var name: String  = ""
    @Persisted var desc: String = ""
    @Persisted var rating: Int = 0
    @Persisted var spentMoney: String = ""
    @Persisted var currency: String = ""
    @Persisted var longitude: Double = 0
    @Persisted var latitude: Double = 0
    @Persisted var transport: Int = 0
    
}
