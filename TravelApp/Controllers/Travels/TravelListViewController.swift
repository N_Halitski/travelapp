//
//  TravelListViewController.swift
//  TravelApp
//
//  Created by Nikita on 05.06.2021.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import RealmSwift

class TravelListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var travels: Results <RealmTravel>! //
    let serverManager = ServerManager()
    var travelNotificationToken: NotificationToken?
    
    var tempTravelNotificationToken: NotificationToken?// нотификация на 1 обект
    
    
    @IBOutlet weak var travelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Realm is here: \(Realm.Configuration.defaultConfiguration.fileURL!.path)")
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addClicked(_:))) // barButtonSystemItem: .add - системная кнопка плюс
        navigationItem.rightBarButtonItem = addButton // говорим navigation что наша созданная кнопка плюсик должна быть справа
        
        
        let travelCellXib = UINib(nibName: "TravelCell", bundle: nil)
        travelTableView.register(travelCellXib, forCellReuseIdentifier: "TravelCell")
        
        travelTableView.delegate = self
        travelTableView.dataSource = self
        
        travelTableView.tableFooterView = UIView()
        travelTableView.backgroundColor = .systemGray4
        
        // тот самый id по которому мы будем понимать, у какого именно юзера с базы данных мы тянем данные
        let userId = Auth.auth().currentUser?.uid
        
        serverManager.dowloadTravels(for: userId!) { [weak self] travels in
            guard let self = self else {return}
            
            let realm = try! Realm()
            
            try! realm.write{
                realm.add(travels, update: .all) // скачали и сохранили в базу. update: .all - для того, чтобы он проверял по id и не было ошибки, если встретиться объект с таким же id. лучше всегда писать так
            }
            
            // скачиваем остановки для каждого тревела
            for travel in travels{
                self.serverManager.downloadStops(forTravelId: travel.id, userId: userId!) { stops in
                    // теперь эти стопы нам нужно добавить в realm
                    try! realm.write{
                        realm.add(stops, update: .all)
                    }
                }
            }
        } onError: { error in
            print("error")
        }
        
        // MARK: пишем слушателяю который слушает изменения всех трэвэлов. для того, чтобы слушать нотификациии нужно выше создать notifactio token
        let realm = try! Realm()
        travels = realm.objects(RealmTravel.self) //выбрал все тревела в базе. тут всегда автоматически обновляется коллекция. не как в массиве, где нужно самому все добавлять или удалять. храним в глобальной переменной в самом верху
        travelNotificationToken = travels.observe { changes in
            print("Changes!!!") // она сработает тогда, когда что-то произойдет с треевлом. не важно что
            
            // дальше нужно понять, кавкие именно произошли изменения, нужен switch
            switch changes{
            
            case .initial(_):
                break // ничего не делай
            case .update(_, deletions: let deletions, insertions: let insertions, modifications: let modifications):
                print("deletions: \(deletions), insertions: \(insertions), modifications: \(modifications) ")
                self.travelTableView.reloadData()
            case .error(_):
                break
            }
        }
    }
    
    
    @objc func addClicked(_ sender: Any){
        let alertVC = UIAlertController(title: "Добавить новое путешествие", message: "можно ничего не писать", preferredStyle: .alert)
        
        // настраиваем кнопки для алерта
        let okAction = UIAlertAction(title: "Сохранить", style: .default) { action in
            print("юезра нажал кнопку Сохранить")
            // создаем по нажатию на копку кормашек и заполлняем его
            
            if let name = alertVC.textFields?[0].text,
               let description = alertVC.textFields?[1].text { // посмотрели что лежит в первом и втором тексфилде textFields?[0]
                let id = UUID().uuidString
                let userId = Auth.auth().currentUser?.uid // создаем  юзер айди
                
                
                
                
                let travel = RealmTravel()
                travel.id = id
                travel.userId = userId!
                travel.name = name
                travel.desc = description
                // по нажатию добавим тревел в реалм
                let realm = try! Realm()
                try! realm.write{
                    realm.add(travel)
                }
                self.serverManager.sandTravelToServer(travel)
                
                
                
                
                
                
                
                
                //  MARK: как слушать 1 объект
                
                //                // разница медлу нотфиикации на 1 и объект и коллецию. нам возвращаются индексы. у объекта возвращаются изсенённые свойства
                //                self.tempTravelNotificationToken = travel.observe{ change in
                //                    switch change{
                //
                //                    case .error(_):
                //                        break
                //                    case .change(_, let properties):
                //                        // можно пробежаться по всем измененныйм свойтсвам
                //                        for property in properties{
                //                            print("Было изменено поле: \(property)")
                //                        }
                //                    case .deleted:
                //                        break
                //                    }
                //                }
                
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { action in // style: .cancel - жирный стиль кнопки
            print("юезра нажал кнопку Отмена")
        }
        // теперь их нужно связать с нашим алертом
        
        alertVC.addAction(okAction)
        alertVC.addAction(cancelAction)
        
        alertVC.addTextField { textField in
            textField.placeholder = "Введите название путешествия ..."
            
        }
        alertVC.addTextField { textField in
            textField.placeholder = "Описание"
        }
        present(alertVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // высота ячейки
        return 150
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = travelTableView.dequeueReusableCell(withIdentifier: "TravelCell") as! TravelCell
        let travel = travels[indexPath.row]
        cell.travelNameLabel.text = travel.name
        cell.descriptionTravelLabel.text = travel.desc
        let rating = getAverageRating(for: travel) // переменная для функции
        
        for i in 0..<rating{
            
            cell.stars[i].isHighlighted = i < rating
        }
        
        return cell
    }
    
    // функция по высчету среднего арифметичсемкого для тревела
    func getAverageRating(for travel: RealmTravel) -> Int {
        
    
        var result = 0
        var sum = 0 // сумма всех раздеенное на их кол-во
        
        let realm = try! Realm()
        let stops = realm.objects(RealmStop.self).filter("travelId == %@", travel.id) // выбрали все стопы
        
        if stops.count == 0{
            return 0 
        }
        for stop in stops{
            sum += stop.rating
        }
        result = sum / stops.count
        return result
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopsVC = StopsViewController()
        stopsVC.travel = travels[indexPath.row]
        self.navigationController?.pushViewController(stopsVC, animated: true)
    }
}

